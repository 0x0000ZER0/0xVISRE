CC 	=gcc
CXX 	=g++
SAN	=-fsanitize=leak,address,undefined 
FLAGS  	=-Wall -Wextra 
LIBS	=-lX11 -lGL -lGLEW -lm

SRC     = main.c x11.c gl.c log.c file.c math.c
OUT     = visre

debug:
	$(CC) $(SRC) $(FLAGS) $(LIBS) $(SAN) -o $(OUT)

release:
	$(CC) $(SRC) $(FLAGS) $(LIBS) -o $(OUT)

run:
	./$(OUT) $(p)

pack:
	upx demo/$(n) -o demo/$(n)-upx

.PHONY:demo
demo:
	$(CC) demo/$(n).c -Wall -Wextra -static -o demo/$(n)

.PHONY:demo_cpp
demo_cpp:
	$(CXX) demo/$(n).cpp -Wall -Wextra $(SAN) -o demo/$(n)
