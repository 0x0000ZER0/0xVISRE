#include <GL/glew.h>
#include <GL/gl.h>

#include <string.h>
#include <malloc.h>

#include <png.h>

#include "defs.h"

static void
gl_shader_new(u32 *id, u32 type, const char *src, s32 len)
{
        *id = glCreateShader(type);
        glShaderSource(*id, 1, &src, &len);
        glCompileShader(*id);

        int res;
        glGetShaderiv(*id, GL_COMPILE_STATUS, &res);
        if (res == 0) {
                char msg[256];

                glGetShaderInfoLog(*id, sizeof (msg), NULL, msg);
                log_err("shader failure, %s", msg);
        }
}

static void GLAPIENTRY 
gl_dbg_msg(GLenum src, GLenum type, GLuint id, GLenum sev, GLsizei len, const GLchar* msg, const void *param) 
{
	(void)src;
	(void)id;
	(void)sev;
	(void)len;
	(void)param;

	log_err("OpenGL Report [%u], %s\b", type, msg);
}

u32
gl_ctx_init(gl_ctx *ctx)
{
        GLenum glew_res;
        glew_res = glewInit();
        if (glew_res != GLEW_OK)
		return GL_CTX_INIT_ERR_GLEW;

        glEnable(GL_DEPTH_TEST);

	glEnable(GL_DEBUG_OUTPUT);
    	glDebugMessageCallback(gl_dbg_msg, NULL);

	glEnable(GL_PROGRAM_POINT_SIZE);
	glEnable(GL_BLEND);

	// text
	{
//		glGenTextures(1, &ctx->t_id);
//		glBindTexture(GL_TEXTURE_2D, ctx->t_id);
//		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, png_w, png_h, 0, GL_RGB, GL_UNSIGNED_BYTE, png_data);
	}

	// blob
	{
		glGenBuffers(1, &ctx->b_id);
		glBindBuffer(GL_ARRAY_BUFFER, ctx->b_id);
		glBufferData(GL_ARRAY_BUFFER, ctx->size * sizeof (gl_byte), NULL, GL_DYNAMIC_DRAW); 

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2,  GL_FLOAT, GL_FALSE, sizeof (gl_byte), (const void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4,  GL_FLOAT, GL_FALSE, sizeof (gl_byte), (const void*)(sizeof (float) * 2));

        	u32 v_id, f_id;

		file v_file;
		v_file.path = "shader/blob.vert";
		v_file.mode = FILE_MODE_R | FILE_MODE_M;
		file_open(&v_file);
		{
        		gl_shader_new(&v_id, GL_VERTEX_SHADER, (const char*)v_file.data, v_file.size);
		}
		file_unmap(&v_file);

		file f_file;
		f_file.path = "shader/blob.frag";
		f_file.mode = FILE_MODE_R | FILE_MODE_M;
		file_open(&f_file);
		{
        		gl_shader_new(&f_id, GL_FRAGMENT_SHADER, (const char*)f_file.data, f_file.size);
		}
		file_unmap(&f_file);

        	ctx->p_b_id = glCreateProgram();
        	glAttachShader(ctx->p_b_id, v_id);
        	glAttachShader(ctx->p_b_id, f_id);

        	s32 log_res;

        	glLinkProgram(ctx->p_b_id);
		glGetProgramiv(ctx->p_b_id, GL_LINK_STATUS, &log_res);
        	if (log_res == 0) {
        	        char msg[256];

        	        glGetProgramInfoLog(ctx->p_b_id, sizeof (msg), NULL, msg);
        	        log_err("linker failure, %s", msg);
        	}

        	glValidateProgram(ctx->p_b_id);
        	glGetProgramiv(ctx->p_b_id, GL_VALIDATE_STATUS, &log_res);
        	if (log_res == 0) {
        	        char msg[256];

        	        glGetProgramInfoLog(ctx->p_b_id, sizeof (msg), NULL, msg);
        	        log_err("validation failure, %s", msg);
        	}

        	glDeleteShader(v_id);
        	glDeleteShader(f_id);

		ctx->u_d_id = glGetUniformLocation(ctx->p_b_id, "diff");
		if (ctx->u_d_id == -1)
			return GL_CTX_INIT_ERR_UNI;

		ctx->blob = malloc(ctx->size * sizeof (gl_byte));
		if (ctx->blob == NULL)
			return GL_CTX_INIT_ERR_BLOB;
	}

	// cube
	{
		u32 size;
		size = (ctx->size - 2) * sizeof (vec3);

		glGenBuffers(1, &ctx->c_id);
		glBindBuffer(GL_ARRAY_BUFFER, ctx->c_id);
		glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_DYNAMIC_DRAW); 

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3,  GL_FLOAT, GL_FALSE, sizeof (vec3), (const void*)0);

        	u32 v_id, f_id;

		file v_file;
		v_file.path = "shader/cube.vert";
		v_file.mode = FILE_MODE_R | FILE_MODE_M;
		file_open(&v_file);
		{
        		gl_shader_new(&v_id, GL_VERTEX_SHADER, (const char*)v_file.data, v_file.size);
		}
		file_unmap(&v_file);

		file f_file;
		f_file.path = "shader/cube.frag";
		f_file.mode = FILE_MODE_R | FILE_MODE_M;
		file_open(&f_file);
		{
        		gl_shader_new(&f_id, GL_FRAGMENT_SHADER, (const char*)f_file.data, f_file.size);
		}
		file_unmap(&f_file);

        	ctx->p_c_id = glCreateProgram();
        	glAttachShader(ctx->p_c_id, v_id);
        	glAttachShader(ctx->p_c_id, f_id);

        	s32 log_res;

        	glLinkProgram(ctx->p_c_id);
		glGetProgramiv(ctx->p_c_id, GL_LINK_STATUS, &log_res);
        	if (log_res == 0) {
        	        char msg[256];

        	        glGetProgramInfoLog(ctx->p_c_id, sizeof (msg), NULL, msg);
        	        log_err("linker failure, %s", msg);
        	}

        	glValidateProgram(ctx->p_c_id);
        	glGetProgramiv(ctx->p_c_id, GL_VALIDATE_STATUS, &log_res);
        	if (log_res == 0) {
        	        char msg[256];

        	        glGetProgramInfoLog(ctx->p_c_id, sizeof (msg), NULL, msg);
        	        log_err("validation failure, %s", msg);
        	}

        	glDeleteShader(v_id);
        	glDeleteShader(f_id);

		ctx->u_o_c_id = glGetUniformLocation(ctx->p_c_id, "u_ori");
		if (ctx->u_o_c_id == -1)
			return GL_CTX_INIT_ERR_UNI;

		ctx->cube = malloc(size);
		if (ctx->cube == NULL)
			return GL_CTX_INIT_ERR_CUBE;
	}

	// sphere
	{
        	u32 v_id, f_id;

		file v_file;
		v_file.path = "shader/sphere.vert";
		v_file.mode = FILE_MODE_R | FILE_MODE_M;
		file_open(&v_file);
		{
        		gl_shader_new(&v_id, GL_VERTEX_SHADER, (const char*)v_file.data, v_file.size);
		}
		file_unmap(&v_file);

		file f_file;
		f_file.path = "shader/sphere.frag";
		f_file.mode = FILE_MODE_R | FILE_MODE_M;
		file_open(&f_file);
		{
        		gl_shader_new(&f_id, GL_FRAGMENT_SHADER, (const char*)f_file.data, f_file.size);
		}
		file_unmap(&f_file);

        	ctx->p_s_id = glCreateProgram();
        	glAttachShader(ctx->p_s_id, v_id);
        	glAttachShader(ctx->p_s_id, f_id);

        	s32 log_res;

        	glLinkProgram(ctx->p_s_id);
		glGetProgramiv(ctx->p_s_id, GL_LINK_STATUS, &log_res);
        	if (log_res == 0) {
        	        char msg[256];

        	        glGetProgramInfoLog(ctx->p_s_id, sizeof (msg), NULL, msg);
        	        log_err("linker failure, %s", msg);
        	}

        	glValidateProgram(ctx->p_s_id);
        	glGetProgramiv(ctx->p_s_id, GL_VALIDATE_STATUS, &log_res);
        	if (log_res == 0) {
        	        char msg[256];

        	        glGetProgramInfoLog(ctx->p_s_id, sizeof (msg), NULL, msg);
        	        log_err("validation failure, %s", msg);
        	}

        	glDeleteShader(v_id);
        	glDeleteShader(f_id);

		ctx->u_o_s_id = glGetUniformLocation(ctx->p_s_id, "u_ori");
		if (ctx->u_o_s_id == -1)
			return GL_CTX_INIT_ERR_UNI;

		ctx->u_c_id = glGetUniformLocation(ctx->p_s_id, "u_cnt");
		if (ctx->u_c_id == -1)
			return GL_CTX_INIT_ERR_UNI;
	}

	// map
	{
		u32 size;
		size = ctx->size * sizeof (vec3) * 4;

		glGenBuffers(1, &ctx->m_id);
		glBindBuffer(GL_ARRAY_BUFFER, ctx->m_id);
		glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_DYNAMIC_DRAW); 

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3,  GL_FLOAT, GL_FALSE, sizeof (vec3), (const void*)0);

		// fix this
		u32 w = 600, h = 600;

		u32 ilen;
		//ilen = (w * 2) * (h - 1) * sizeof (u32); // fix me
		ilen = (w * 2) * (h) * sizeof (u32); // fix me
		ctx->i_map = malloc(ilen);
		if (ctx->i_map == NULL)
			return GL_CTX_INIT_ERR_MAP;

		// fix me, please
		u32 i;
		i = 0;
		for (u32 y = 0; y < w; ++y) {
			for (u32 x = 0; x < h; ++x) {
				ctx->i_map[i] = (y + 0) * w + x;
				++i;
				ctx->i_map[i] = (y + 1) * w + x;
				++i;
			}
		}


		glGenBuffers(1, &ctx->i_m_id);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ctx->i_m_id);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, ilen, ctx->i_map, GL_STATIC_DRAW);

        	u32 v_id, f_id;

		file v_file;
		v_file.path = "shader/map.vert";
		v_file.mode = FILE_MODE_R | FILE_MODE_M;
		file_open(&v_file);
		{
        		gl_shader_new(&v_id, GL_VERTEX_SHADER, (const char*)v_file.data, v_file.size);
		}
		file_unmap(&v_file);

		file f_file;
		f_file.path = "shader/map.frag";
		f_file.mode = FILE_MODE_R | FILE_MODE_M;
		file_open(&f_file);
		{
        		gl_shader_new(&f_id, GL_FRAGMENT_SHADER, (const char*)f_file.data, f_file.size);
		}
		file_unmap(&f_file);

        	ctx->p_m_id = glCreateProgram();
        	glAttachShader(ctx->p_m_id, v_id);
        	glAttachShader(ctx->p_m_id, f_id);

        	s32 log_res;

        	glLinkProgram(ctx->p_m_id);
		glGetProgramiv(ctx->p_m_id, GL_LINK_STATUS, &log_res);
        	if (log_res == 0) {
        	        char msg[256];

        	        glGetProgramInfoLog(ctx->p_m_id, sizeof (msg), NULL, msg);
        	        log_err("linker failure, %s", msg);
        	}

        	glValidateProgram(ctx->p_m_id);
        	glGetProgramiv(ctx->p_m_id, GL_VALIDATE_STATUS, &log_res);
        	if (log_res == 0) {
        	        char msg[256];

        	        glGetProgramInfoLog(ctx->p_m_id, sizeof (msg), NULL, msg);
        	        log_err("validation failure, %s", msg);
        	}

        	glDeleteShader(v_id);
        	glDeleteShader(f_id);

		ctx->u_o_m_id = glGetUniformLocation(ctx->p_m_id, "u_ori");
		if (ctx->u_o_m_id == -1)
			return GL_CTX_INIT_ERR_UNI;

		ctx->map = malloc(size);
		if (ctx->map == NULL)
			return GL_CTX_INIT_ERR_MAP;
	}

	return GL_CTX_INIT_OK;
}

void
gl_draw_bg(gl_bg bg)
{
	glViewport(0, 0, bg.w, bg.h);
	
	glClearColor(bg.r, bg.g, bg.b, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void
gl_draw_blob(gl_ctx *ctx, u8 *data, u32 w, u32 h, u8 *can_draw)
{
	glBindBuffer(GL_ARRAY_BUFFER, ctx->b_id);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2,  GL_FLOAT, GL_FALSE, sizeof (gl_byte), (const void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4,  GL_FLOAT, GL_FALSE, sizeof (gl_byte), (const void*)(sizeof (float) * 2));

	glUseProgram(ctx->p_b_id);

	u32 max;
	max = ctx->size;

	gl_byte *T;
	T = ctx->blob;

	if (*can_draw) {
		for (u32 i = 0; i < max; ++i) {
			if (data[i] >= 33 && data[i] <= 126) {
				T[i].col[0] = 0.0f;
				T[i].col[1] = 0.7f;
				T[i].col[2] = 0.9f;
				T[i].col[3] = 1.0f;
			} else if (data[i] == 0x00) {
				T[i].col[0] = 0.2f;
				T[i].col[1] = 0.2f;
				T[i].col[2] = 0.2f;
				T[i].col[3] = 1.0f;
			} else if (data[i] == 0xFF) {
				T[i].col[0] = 0.5f;
				T[i].col[1] = 0.0f;
				T[i].col[2] = 0.9f;
				T[i].col[3] = 1.0f;
			} else {
				T[i].col[0] = 0.0f;
				T[i].col[1] = 0.9f;
				T[i].col[2] = 0.5f;
				T[i].col[3] = 1.0f;
			}
	
			T[i].pos[0] = (f32)(i % w) / w - 0.5f;
			T[i].pos[1] = 0.5f - (f32)(i / w) / h;
		}	
	
		f32 diff;
		diff = (T[1].pos[0] - T[0].pos[0]) * 800.0f;
		glUniform1f(ctx->u_d_id, diff);

		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof (gl_byte) * max, T);

		*can_draw = FALSE;
	}

	glDrawArrays(GL_POINTS, 0, max);
}

void
gl_draw_cube(gl_ctx *ctx, u8 *data, u8 *can_draw, f32 z_a, f32 x_a, u8 norm, f32 cnt, u32 w, u32 h)
{
	glDisableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, ctx->c_id);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3,  GL_FLOAT, GL_FALSE, sizeof (vec3), (const void*)0);

	if (norm) {
		glUseProgram(ctx->p_s_id);
		glUniform1f(ctx->u_c_id, cnt);
	} else {
		glUseProgram(ctx->p_c_id);
	}

	u32 size;
	size = ctx->size;

	vec3 *cube;
	cube = ctx->cube;

	if (*can_draw) {
		mat4 rotx, rotz, rot;

		mat4_zro_idn(rotz);
		mat4_rot_z(rotz, TO_RAD(x_a));

		mat4_zro_idn(rotx);
		mat4_rot_x(rotx, TO_RAD(z_a));

		mat4_zro(rot);
		mat4_mul(rot, rotx, rotz);

		// order: T * R * S
		//mat4 ori;
		//mat4_zro(ori);
		//mat4_mul(ori, tsl, rot);

		// mvp: P * V * M
		//mat4 proj;
		//mat4_zro(proj);
		//mat4_pers(proj, (f32)w / (f32)h, TO_RAD(45.0f), 0.1f, 1000.0f);

		//mat4 view;
		//mat4_zro_idn(view);
		//mat4_tsl(view, (vec3){0.0f, 0.0f, -500.0f});
		//mat4_look_at(view, (vec3){255.0f,255.0f,-255.0f}, (vec3){0.0f,0.0f,0.0f}, (vec3){0.0f, 1.0f, 0.0f});

		//mat4 mvp;
		//mat4_mul(mvp, proj, view);
		//mat4 tmp;
		//mat4_cpy(tmp, mvp);
		//mat4_mul(mvp, tmp, ori);

		//glUniformMatrix4fv(ctx->u_o_c_id, 1, GL_FALSE, &ori[0][0]);
		glUniformMatrix4fv(ctx->u_o_c_id, 1, GL_TRUE, &rot[0][0]);

		for (u32 i = 0; i < size - 2; ++i) {
			//cube[i][0] = (f32)data[i + 0];
			//cube[i][1] = (f32)data[i + 1];
			//cube[i][2] = (f32)data[i + 2];
			cube[i][0] = (f32)data[i + 0] / 255.0f - 0.5f;
			cube[i][1] = (f32)data[i + 1] / 255.0f - 0.5f;
			cube[i][2] = (f32)data[i + 2] / 255.0f - 0.5f;
		}

		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof (vec3) * (size - 2), ctx->cube);

		*can_draw = FALSE;
	}

	glDrawArrays(GL_POINTS, 0, size - 2);
}

void
gl_draw_map(gl_ctx *ctx, u8 *data, u32 w, u32 h, u8 *can_draw, f32 x_a, f32 z_a)
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glBindBuffer(GL_ARRAY_BUFFER, ctx->m_id);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3,  GL_FLOAT, GL_FALSE, sizeof (vec3), (const void*)0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ctx->i_m_id);

	glUseProgram(ctx->p_m_id);

	vec3 *map;
	map = ctx->map;

	u32 curr;

	if (can_draw) {
		mat4 rotx, rotz, rot;

		mat4_zro_idn(rotz);
		mat4_rot_y(rotz, TO_RAD(x_a - 220.0f));

		mat4_zro_idn(rotx);
		mat4_rot_x(rotx, TO_RAD(z_a + 45.0f));

		mat4_zro(rot);
		mat4_mul(rot, rotx, rotz);

		mat4 view;
		mat4_zro_idn(view);
		
		vec3 pos;
		pos[0] = 0.0f;
		pos[1] = 0.0f;
		pos[2] = -0.7f;
		mat4_tsl(view, pos);

		// order: P * V * M
		mat4 mvp;
		mat4_zro_idn(mvp);
		mat4_mul(mvp, view, rot);

		glUniformMatrix4fv(ctx->u_o_c_id, 1, GL_TRUE, &mvp[0][0]);

		for (u32 y = 0; y < h; ++y) {
			for (u32 x = 0; x < w; ++x) {
				curr = y * w + x;

				map[curr][0] = ((f32)x / (f32)(w / 20) - 1.0f) * 2.0f;
				map[curr][1] = (f32)data[curr] / 500.0f - 0.7f;	       		
				map[curr][2] = ((f32)y / (f32)(h / 20) - 1.0f) * 2.0f;
			}
		}

		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof (vec3) * w * h, ctx->map);

		for (u32 i = 0; i < h - 1; ++i) 
			glDrawElements(GL_TRIANGLE_STRIP, 2 * w, GL_UNSIGNED_INT, (void*)(2 * w * sizeof (u32) * i));

		*can_draw = FALSE;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void
gl_ctx_free(gl_ctx *ctx)
{
	free(ctx->i_map);
	free(ctx->map);
	free(ctx->cube);
	free(ctx->blob);
}
