#include "defs.h"

int
main(int argc, char *argv[])
{
	if (argc < 2) {
		log_err("need a binary file");
		return 1;
	}

	u32 res;

	file bin;
	bin.path = argv[1];
	bin.mode = FILE_MODE_R | FILE_MODE_M;
	res = file_open(&bin);
	if (res != FILE_OPEN_OK) {
		log_err("failed to open '%s', res[%u]", bin.path, res);
		return 1;
	}
	file_close(&bin);

	log_inf("opened the file, size:[%u], len:[%u]", bin.size, bin.d_len);

	x11_ctx x11;
	x11.x  	   = 100;
	x11.y  	   = 100;
	x11.width  = 600;
	x11.height = 600;
	x11.title  = "binary visualizer";
	res = x11_ctx_init(&x11);
	if (res != X11_CTX_INIT_OK) {
		log_err("failed to initialize X11, res[%u]", res);
		goto _EXIT_FILE_;
	}

	u32 max;
	max = x11.width / 2 * x11.height / 2;
	if (bin.size < max)
		max = bin.size;

	log_inf("going to process [%u] bytes", max);

	gl_ctx gl;
	gl.size = max;
	res = gl_ctx_init(&gl);
	if (res != GL_CTX_INIT_OK) {
		log_err("failed to initialize GL, res[%u]", res);
		goto _EXIT_X11_;
	}

	log_gl_ver();

	gl_bg bg;
	bg.r = 0.0f;
	bg.g = 0.0f;
	bg.b = 0.0f;

	s32 s;
	s = 0;

	f32 cnt;
	cnt = -1.0f;

	enum {
		BLOB,
		CUBE,
		SPHERE,
		MAP
	};

	u8 can_draw, mode;
	can_draw = TRUE;
	mode = BLOB;

	u32 c, m;
	c = 0, m = 12;

	f32 z_a, x_a, inc;
	z_a = x_a = 0.0f, inc = 5.0f;

	x11_att att;
	att.width  = x11.width;
	att.height = x11.height;
	while (TRUE) {
		x11_att_query(&x11, &att);

		bg.w = att.width;
		bg.h = att.height;
		gl_draw_bg(bg);

		switch (att.type) {
		case X11_ATT_CLIENT:
			if (att.data == x11.a_del)
				goto _EXIT_GL_;
			break;
		case X11_ATT_KEY_PRESS:
			switch (att.code) {
			case X11_KEY_CODE_ESC:
				goto _EXIT_GL_;
			case X11_KEY_CODE_LEFT:
				x_a -= inc;
				can_draw = TRUE;
				break;
			case X11_KEY_CODE_RIGHT:
				x_a += inc;	
				can_draw = TRUE;
				break;
			case X11_KEY_CODE_DOWN:
				z_a += inc;	
				can_draw = TRUE;
				break;
			case X11_KEY_CODE_UP:
				z_a -= inc;	
				can_draw = TRUE;
				break;
			case X11_KEY_CODE_1:
				mode = BLOB;
				can_draw = TRUE;
				break;
			case X11_KEY_CODE_2:
				mode = CUBE;
				can_draw = TRUE;
				break;
			case X11_KEY_CODE_3:
				mode = SPHERE;
				can_draw = TRUE;
				break;
			case X11_KEY_CODE_4:
				mode = MAP;
				can_draw = TRUE;
				break;
			}
			break;

		case X11_ATT_MOUSE_PRESS:
			switch (att.button) {
			case X11_MOUSE_CODE_UP:
				if (c < bin.size - max) {
					can_draw = TRUE;
					c += att.width / 2 * m;
				}
				break;
			case X11_MOUSE_CODE_DOWN:
				if (c > 0) {
					can_draw = TRUE;
					c -= att.width / 2 * m;
				}
				break;
			}
			break;
		}

		if (mode == BLOB)
			gl_draw_blob(&gl, bin.data + c, att.width / 2 + s, att.height / 2 + s, &can_draw);
		else if (mode == MAP)
			gl_draw_map(&gl, bin.data + c, att.width, att.height, &can_draw, x_a, z_a);
		else
			gl_draw_cube(&gl, bin.data + c, &can_draw, z_a, x_a, mode == SPHERE, cnt, att.width, att.height);

		cnt += 0.01f;

		x11_ctx_update(&x11);
	}

_EXIT_GL_:
	gl_ctx_free(&gl);
_EXIT_X11_:
	x11_ctx_free(&x11);
_EXIT_FILE_:
	file_unmap(&bin);
        return 0;
}
