#include "defs.h"

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

u32
file_open(file *curr)
{
	int flag;

	flag = (curr->mode & 0x3) - 1;
	curr->hnd = open(curr->path, flag);
	if (curr->hnd == -1)
		return FILE_OPEN_ERR_INIT;

	int res;

	struct stat info;
	res = fstat(curr->hnd, &info);
	if (res == -1) {
		close(curr->hnd);
		return FILE_OPEN_ERR_STAT;
	}

	curr->size = info.st_size;

	if (curr->mode & FILE_MODE_M) {
		int page_size;
		page_size = getpagesize();

		curr->d_len = (curr->size + page_size - 1) & ~(page_size - 1);
		curr->data  = mmap(NULL, curr->d_len, PROT_READ, MAP_PRIVATE, curr->hnd, 0);
		if (curr->data == MAP_FAILED) {
			close(curr->hnd);
			return FILE_OPEN_ERR_MAP;
		}
	}

	return FILE_OPEN_OK;
}

inline void
file_close(file *curr)
{
	close(curr->hnd);
}

inline void
file_unmap(file *curr) 
{
	if (curr->hnd != -1)
		close(curr->hnd);

	if (curr->mode & FILE_MODE_M)
		munmap(curr->data, curr->d_len); 
}
