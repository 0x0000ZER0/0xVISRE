#include "defs.h"

#include <stdio.h>
#include <stdarg.h>

void
log_inf(const char *fmt, ...)
{
        printf("INF: ");

        va_list args;
        va_start(args, fmt);
        vprintf(fmt, args);
        va_end(args);

        printf("\n");
}

void
log_err(const char *fmt, ...)
{
        fprintf(stderr, "ERR: ");

        va_list args;
        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);

        fprintf(stderr, "\n");
}

void
log_vec3(vec3 v)
{
        printf("--------- 3 ---------\n");
	printf("%+.03f %+.03f %+.03f\n", v[0], v[1], v[2]);
}

void
log_mat4(mat4 m)
{
        printf("--------- 4x4 ----------\n");
	for (u32 y = 0; y < 4; ++y) {
		for (u32 x = 0; x < 4; ++x)
			printf("%+.02f ", m[y][x]);
        	printf("\n");
	}
}

void
log_gl_ver(void)
{
	log_inf("renderer:[%s], version:[%s], shader:[%s]", 
		glGetString(GL_RENDERER), glGetString(GL_VERSION),
		glGetString(GL_SHADING_LANGUAGE_VERSION));
}
