#include "defs.h"

#include <string.h>

inline f32
vec3_len(vec3 v)
{
	return sqrtf(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
}

inline void
vec3_nrm(vec3 o, vec3 v)
{
	f32 i;
	i = 1.0f / vec3_len(v);

	o[0] = v[0] * i;
	o[1] = v[1] * i;
	o[2] = v[2] * i;
}

inline f32
vec3_dot(vec3 a, vec3 b)
{
	return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

inline void
vec3_crs(vec3 d, vec3 a, vec3 b)
{
	d[0] = a[1] * b[2] - a[2] * b[1];
	d[1] = a[2] * b[0] - a[0] * b[2];
	d[2] = a[0] * b[1] - a[1] * b[0];
}

inline void
vec3_add(vec3 d, vec3 a, vec3 b)
{
	d[0] = a[0] + b[0];
	d[1] = a[1] + b[1];
	d[2] = a[2] + b[2];
}

inline void
vec3_sub(vec3 d, vec3 a, vec3 b)
{
	d[0] = a[0] - b[0];
	d[1] = a[1] - b[1];
	d[2] = a[2] - b[2];
}

inline void
vec3_mul(vec3 d, vec3 a, vec3 b)
{
	d[0] = a[0] * b[0];
	d[1] = a[1] * b[1];
	d[2] = a[2] * b[2];
}

inline void
mat4_zro(mat4 m)
{
	memset(m, 0, sizeof (mat4));
}

inline void
mat4_cpy(mat4 d, mat4 s)
{
	memcpy(d, s, sizeof (mat4));
}

inline u8
mat4_cmp(mat4 a, mat4 b)
{
	return memcmp(a, b, sizeof (mat4));
}

inline void
mat4_mul(mat4 d, mat4 a, mat4 b)
{
	for (u32 y = 0; y < 4; ++y)
	for (u32 x = 0; x < 4; ++x)
		d[y][x] = a[y][0] * b[0][x] + 
			a[y][1] * b[1][x] + 
			a[y][2] * b[2][x] + 
			a[y][3] * b[3][x];
}

inline void
mat4_idn(mat4 m)
{
	m[0][0] = 1.0f;
	m[1][1] = 1.0f;
	m[2][2] = 1.0f;
	m[3][3] = 1.0f;
}

inline void
mat4_zro_idn(mat4 m)
{
	m[0][0] = 1.0f;
	m[0][1] = 0.0f;
	m[0][2] = 0.0f;
	m[0][3] = 0.0f;

	m[1][0] = 0.0f;
	m[1][1] = 1.0f;
	m[1][2] = 0.0f;
	m[1][3] = 0.0f;

	m[2][0] = 0.0f;
	m[2][1] = 0.0f;
	m[2][2] = 1.0f;
	m[2][3] = 0.0f;

	m[3][0] = 0.0f;
	m[3][1] = 0.0f;
	m[3][2] = 0.0f;
	m[3][3] = 1.0f;
}

inline void
mat4_mul_vec3(vec3 d, mat4 m, vec3 s)
{
	d[0] = m[0][0] * s[0] + m[1][0] * s[1] + m[2][0] * s[2] + m[3][0];
	d[1] = m[0][1] * s[0] + m[1][1] * s[1] + m[2][1] * s[2] + m[3][1];
	d[2] = m[0][2] * s[0] + m[1][2] * s[1] + m[2][2] * s[2] + m[3][2];
}

inline void
mat4_proj_vec3(vec3 d, mat4 m, vec3 s)
{
	d[0] = m[0][0] * s[0] + m[1][0] * s[1] + m[2][0] * s[2] + m[3][0];
	d[1] = m[0][1] * s[0] + m[1][1] * s[1] + m[2][1] * s[2] + m[3][1];
	d[2] = m[0][2] * s[0] + m[1][2] * s[1] + m[2][2] * s[2] + m[3][2];

	if (m[3][3] != 0.0f) {
		d[0] /= m[3][3];
		d[1] /= m[3][3];
		d[2] /= m[3][3];
	}
}

inline void
mat4_trn(mat4 d, mat4 s)
{
	for (u32 y = 0; y < 4; ++y)
	for (u32 x = 0; x < 4; ++x)
		d[x][y] = s[y][x];
}

inline void
mat4_tsl(mat4 m, vec3 p)
{
	m[3][0] = p[0];
	m[3][1] = p[1];
	m[3][2] = p[2];
}

inline void
mat4_scl(mat4 m, vec3 s)
{
	m[0][0] = s[0];
	m[1][1] = s[1];
	m[2][2] = s[2];
}


inline void
mat4_pers(mat4 m, f32 r, f32 a, f32 n, f32 f)
{
	f32 t;
	t = 1.0f / tanf(a / 2.0f);

	m[0][0] = r * t;
	m[1][1] = t;
	m[2][2] = -(f + n) / (f - n);
	m[3][2] = -1.0f;
	m[2][3] = -(2.0f * f * n) / (f - n);
}

inline void
mat4_look_at(mat4 m, vec3 f, vec3 t, vec3 u)
{
	// forward
	vec3 d;
	vec3_sub(d, t, f);

	vec3 n;
	vec3_nrm(n, d);

	// right
	vec3 o;
	vec3_crs(o, n, u);

	vec3 r;
	vec3_nrm(r, o);

	// up
	vec3 c;
	vec3_crs(c, n, r);

	m[0][0] = r[0];
	m[0][1] = r[1];
	m[0][2] = r[2];
	m[0][3] = 0.0f;

	m[1][0] = c[0];
	m[1][1] = c[1];
	m[1][2] = c[2];
	m[1][3] = 0.0f;

	m[2][0] = d[0];
	m[2][1] = d[1];
	m[2][2] = d[2];
	m[2][3] = 0.0f;

	m[2][0] = f[0];
	m[2][1] = f[1];
	m[2][2] = f[2];
	m[2][3] = 1.0f;
}

inline void
mat4_rot_x(mat4 m, f32 a)
{
	m[1][1] = cosf(a);
	m[1][2] = -sinf(a);

	m[2][1] = sinf(a);
	m[2][2] = cosf(a);
}

inline void
mat4_rot_y(mat4 m, f32 a)
{
	m[0][0] = cosf(a);
	m[0][2] = sinf(a);

	m[2][0] = -sinf(a);
	m[2][2] = cosf(a);
}

inline void
mat4_rot_z(mat4 m, f32 a)
{
	m[0][0] = cosf(a);
	m[0][1] = -sinf(a);

	m[1][0] = sinf(a);
	m[1][1] = cosf(a);
}
