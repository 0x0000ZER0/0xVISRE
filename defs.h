#ifndef __DEFS_H__
#define __DEFS_H__

#include <GL/glx.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <math.h>

#include "types.h"

// -------------- MATH --------------

#define TO_RAD(a) ((a) * M_PI / 180.0f)

enum {
	MAT4_CMP_EQ = 0,
	MAT4_CMP_NE
};

f32
vec3_len(vec3);

void
vec3_nrm(vec3, vec3);

void
vec3_add(vec3, vec3, vec3);

void
vec3_sub(vec3, vec3, vec3);

void
vec3_mul(vec3, vec3, vec3);

f32
vec3_dot(vec3, vec3);

void
vec3_crs(vec3, vec3, vec3);

void
mat4_zro(mat4);

void
mat4_idn(mat4);

void
mat4_zro_idn(mat4);

void
mat4_cpy(mat4, mat4);

u8
mat4_cmp(mat4, mat4);

void
mat4_mul(mat4, mat4, mat4);

void
mat4_mul_vec3(vec3, mat4, vec3);

void
mat4_proj_vec3(vec3, mat4, vec3);

void
mat4_tsl(mat4, vec3);

void
mat4_rot_x(mat4, f32);

void
mat4_rot_y(mat4, f32);

void
mat4_rot_z(mat4, f32);

void
mat4_scl(mat4, vec3);

void
mat4_pers(mat4, f32, f32, f32, f32);

void
mat4_look_at(mat4, vec3, vec3, vec3);

// -------------- LOG --------------

void
log_inf(const char*, ...);

void
log_err(const char*, ...);

void
log_vec3(vec3);

void
log_mat4(mat4);

void
log_gl_ver(void);

// -------------- X11 --------------

#define X11_ATT_DRAW 		Expose
#define X11_ATT_KEY_PRESS 	KeyPress
#define X11_ATT_MOUSE_PRESS 	ButtonPress
#define X11_ATT_CLIENT 		ClientMessage

#define X11_MOUSE_CODE_LEFT 	1
#define X11_MOUSE_CODE_MIDDLE 	2
#define X11_MOUSE_CODE_RIGHT 	3
#define X11_MOUSE_CODE_UP 	4
#define X11_MOUSE_CODE_DOWN 	5

#define X11_KEY_CODE_ESC 	9
#define X11_KEY_CODE_N 		57
#define X11_KEY_CODE_MINUS 	82
#define X11_KEY_CODE_4 		83
#define X11_KEY_CODE_PLUS 	86
#define X11_KEY_CODE_1 		87
#define X11_KEY_CODE_2 		88
#define X11_KEY_CODE_3 		89
#define X11_KEY_CODE_UP 	111
#define X11_KEY_CODE_LEFT 	113
#define X11_KEY_CODE_RIGHT 	114
#define X11_KEY_CODE_DOWN 	116

#define X11_KEY_STATE_SHIFT 	ShiftMask

typedef struct {
	u32 type;
	u32 state;
	u32 code;
	u32 button;
	u64 data;
	u32 width;
	u32 height;
} x11_att;

typedef struct {
	s32 x;
	s32 y;

	u32 width;
	u32 height;
	
	const char *title;

	// --- internal ---
	Display 	*dpy;
	Window 		root;
	XVisualInfo 	*vis;
	Colormap 	cmap;
	Window 		win;
	Atom 		a_del;
	GLXContext 	glx;
} x11_ctx;

enum {
	X11_CTX_INIT_OK = 0,
	X11_CTX_INIT_ERR_DPY,
	X11_CTX_INIT_ERR_VIS
};

void
x11_att_query(x11_ctx*, x11_att*);

u32
x11_ctx_init(x11_ctx*);

void
x11_ctx_update(x11_ctx*);

void
x11_ctx_free(x11_ctx*);

// -------------- GL --------------

typedef struct {
	vec2 pos;
	vec4 col;
} gl_byte;

typedef struct {
	f32 r;
	f32 g;
	f32 b;
	u32 w;
	u32 h;
} gl_bg;

typedef struct {
	u32 	size;

	// --- internal ---
	gl_byte *blob;
	vec3 	*cube;
	vec3 	*map;

	u32 *i_map;

	u32 b_id;
	u32 c_id;
	u32 m_id;

	u32 i_m_id;

	u32 t_id;

	s32 u_d_id;
	s32 u_c_id;
	s32 u_o_c_id;
	s32 u_o_s_id;
	s32 u_o_m_id;

	u32 p_b_id;
	u32 p_c_id;
	u32 p_s_id;
	u32 p_m_id;
} gl_ctx;

enum {
	GL_CTX_INIT_OK = 0,
	GL_CTX_INIT_ERR_GLEW,
	GL_CTX_INIT_ERR_UNI,
	GL_CTX_INIT_ERR_BLOB,
	GL_CTX_INIT_ERR_CUBE,
	GL_CTX_INIT_ERR_MAP
};

u32
gl_ctx_init(gl_ctx*);

void
gl_ctx_free(gl_ctx*);

void
gl_draw_bg(gl_bg);

void
gl_draw_blob(gl_ctx*, u8*, u32, u32, u8*);

void
gl_draw_cube(gl_ctx*, u8*, u8*, f32, f32, u8, f32, u32, u32);

void
gl_draw_map(gl_ctx*, u8*, u32, u32, u8*, f32, f32); 

// -------------- FILE --------------

enum {
	FILE_MODE_R = 0x1,
	FILE_MODE_W = 0x2,
	FILE_MODE_M = 0x4
};

typedef struct {
	const char *path;
	
	u32 mode;
	u32 size;
	u8  *data;
	u32 d_len;

	// --- internal ---
	s32 hnd; 
} file;

enum {
	FILE_OPEN_OK = 0,
	FILE_OPEN_ERR_INIT,
	FILE_OPEN_ERR_STAT,
	FILE_OPEN_ERR_MAP,
};

u32
file_open(file*);

void
file_close(file*);

void
file_unmap(file*);

#endif
