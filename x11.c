#include "defs.h"

#include <X11/X.h>
#include <X11/Xlib.h>

#include <GL/glx.h>

u32
x11_ctx_init(x11_ctx *ctx)
{
        ctx->dpy = XOpenDisplay(NULL);
        if (ctx->dpy == NULL)
                return X11_CTX_INIT_ERR_DPY;

        ctx->root = DefaultRootWindow(ctx->dpy);

        int att[] = {
                GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None
        };

        ctx->vis = glXChooseVisual(ctx->dpy, 0, att);  
        if (ctx->vis == NULL)
                return X11_CTX_INIT_ERR_VIS;

        ctx->cmap = XCreateColormap(ctx->dpy, ctx->root, ctx->vis->visual, None);

        XSetWindowAttributes x11_swa;
        x11_swa.colormap   = ctx->cmap;
        x11_swa.event_mask = ExposureMask | KeyPressMask | KeyReleaseMask | 
		KeymapStateMask | PointerMotionMask | ButtonPressMask | 
		ButtonReleaseMask | EnterWindowMask | LeaveWindowMask;

        ctx->win = XCreateWindow(ctx->dpy, ctx->root, ctx->x, ctx->y, ctx->width, 
			ctx->height, 0, ctx->vis->depth, InputOutput, 
			ctx->vis->visual, CWColormap | CWEventMask, &x11_swa);

        XMapWindow(ctx->dpy, ctx->win);
        XStoreName(ctx->dpy, ctx->win, ctx->title);

	ctx->a_del = XInternAtom(ctx->dpy, "WM_DELETE_WINDOW", False);
	XSetWMProtocols(ctx->dpy, ctx->win, &ctx->a_del, 1);

        ctx->glx = glXCreateContext(ctx->dpy, ctx->vis, NULL, GL_TRUE);
        glXMakeCurrent(ctx->dpy, ctx->win, ctx->glx);

	return X11_CTX_INIT_OK;
}

void
x11_ctx_update(x11_ctx *ctx)
{
	glXSwapBuffers(ctx->dpy, ctx->win);

	//XEvent ev = {0};
	//ev.type = Expose;
	//ev.xexpose.window = ctx->win;

	//XSendEvent(ctx->dpy, ctx->win, False, ExposureMask, &ev);
}

void
x11_ctx_free(x11_ctx *ctx)
{
	glXMakeCurrent(ctx->dpy, None, NULL);
	glXDestroyContext(ctx->dpy, ctx->glx);
	
	XDestroyWindow(ctx->dpy, ctx->win);
	XFreeColormap(ctx->dpy, ctx->cmap);
	XFree(ctx->vis);
	XCloseDisplay(ctx->dpy);
}

void
x11_att_query(x11_ctx *ctx, x11_att *att)
{
	if (XPending(ctx->dpy) <= 0)
		return;

        XEvent x11_ev;
        XWindowAttributes x11_attrs;

        XNextEvent(ctx->dpy, &x11_ev);
	
	att->type  	= x11_ev.type;
	att->state 	= x11_ev.xkey.state;
	att->code  	= x11_ev.xkey.keycode;
	att->button 	= x11_ev.xbutton.button;
	att->data 	= x11_ev.xclient.data.l[0];

	if (x11_ev.type == Expose) {
        	XGetWindowAttributes(ctx->dpy, ctx->win, &x11_attrs);

		att->width 	= x11_attrs.width;
		att->height 	= x11_attrs.height;
	}
}

