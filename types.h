#ifndef __TYPES_H__
#define __TYPES_H__

#include <stdint.h>

#define FALSE 0
#define TRUE  1

typedef int8_t 	 s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;
typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef float 	 f32;

typedef f32 vec2[2];
typedef f32 vec3[3];
typedef f32 vec4[4];

typedef f32 mat4[4][4];

#endif
