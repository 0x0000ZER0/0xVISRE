#version 330 core

in vec3 o_pos;

out vec4 col;

uniform float u_cnt;

void
main(void)
{
 	col = vec4(abs(sin(1.0 - o_pos.x + u_cnt)), abs(cos(o_pos.y + u_cnt)), abs(sin(o_pos.z + u_cnt)), 1.0);
}
