#version 330 core

in vec4 p_col;

out vec4 col;

void
main(void)
{
	col = p_col;
}
