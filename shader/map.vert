#version 330 core

layout(location = 0) 
in vec3 pos;

uniform mat4 u_ori;

out float o_y;

void
main(void)
{
 	gl_Position = u_ori * vec4(pos.xyz, 1.0);
	o_y = pos.y;
}
