#version 330 core

layout(location = 0) 
in vec3 pos;

out vec3 o_pos;

uniform mat4 u_ori;

void
main(void)
{
	vec4 prod = u_ori * vec4(pos.xyz, 1.0);

	vec3 scale = vec3(0.5, 0.5, 0.5);
	vec3 norm = normalize(vec3(prod.xyz)) * scale;

 	gl_Position  = vec4(norm.xyz, 1.0);
	gl_PointSize = 1.5;

	o_pos = pos;
}
