#version 330 core

layout(location = 0) 
in vec2 pos;

layout(location = 1)
in vec4 col;

uniform float diff;

out vec4 p_col;

void
main(void)
{
 	gl_Position  = vec4(pos.xy, 0.0, 1.0);
	gl_PointSize = diff;

	p_col = col;
}
