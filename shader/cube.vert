#version 330 core

layout(location = 0) 
in vec3 pos;

uniform mat4 u_ori;

void
main(void)
{
 	gl_Position  = u_ori * vec4(pos.xyz, 1.0);
	gl_PointSize = 1.5;
}
