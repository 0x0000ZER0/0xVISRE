#version 330 core

in float o_y;

void
main(void)
{
	float o;
	o = o_y + 0.7;

	float c;
	c = o * 1.5 + 0.25;
 	
	gl_FragColor = vec4(o, c, o, 1.0);
}
